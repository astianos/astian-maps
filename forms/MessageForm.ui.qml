import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
   id: root
   property alias messageText: messageText
   property alias messageTitle: messageTitle
   property alias button: button

   Rectangle{
            id: tabRectangle
            y: 20
            height: messageTitle.height * 2
            color: "#46a2da"
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.left: parent.left
            anchors.right: parent.Right

            Label{
                id: messageTitle
                color: "#ffffff"
                text: qsTr("Type")
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
   }

   Item {
       anchors.rightMargin: 20
       anchors.leftMargin: 20
       anchors.bottomMargin: 20
       anchors.topMargin: 20
       anchors.bottom: parent.bottom
       anchors.left: parent.left
       anchors.right: parent.right
       anchors.top: tabRectangle.bottom

       ColumnLayout {
           id: columnLayout1
           spacing: 20
           anchors.fill: parent

           Label {
               id: messageText
               text: qsTr("Message")
               Layout.fillWidth: true
               horizontalAlignment: Text.AlignHCenter
               wrapMode:  Text.WordWrap
               textFormat:  Text.RichText
           }

           Button{
               id: button
               text: qsTr("Ok")
               Layout.alignment: Qt.AlignCenter
           }

           Item {
               Layout.fillHeight: true
           }
       }
   }
}
